﻿using System;
using System.Collections.Generic;
using System.Text;
using static SilverBars.Common;
using System.Linq;

namespace SilverBars
{
    class OrderService
    {
        private readonly List<Order> orders;

        public OrderService()
        {
            orders = new List<Order>();
        }

        public string Register(string userId, OrderTypes orderType, double quantity, double price)
        {

            int id = orders.Count + 1;
            string result = id.ToString();

            try
            {
                orders.Add(new Order
                {
                    Id = id,
                    UserId = userId,
                    OrderType = orderType,
                    Quantity = quantity,
                    Price = price

                });

            }
            catch (Exception)
            {
                result = "Failed";
            }

            return result;
        }


        public string Cancel(int id)
        {
            string result = "Cancelled";
            try
            {
                orders.RemoveAt(orders.FindIndex(o => o.Id == id));

            }
            catch (Exception)
            {
                result = "Failed";
            }

            return result;
        }

        public IEnumerable<OrderSummary> GetOrderSummary()
        {
            var summarySell = (from o in orders
                              where o.OrderType == OrderTypes.SELL
                              select new 
                              {
                                  OrderType = OrderTypes.SELL.ToString(),
                                  Quantity = orders.Where(n => n.Price == o.Price
                                                        && n.OrderType == o.OrderType).Sum(s => s.Quantity),
                                  Price = o.Price
                              }).Distinct().OrderBy(s => s.Price);

            var summaryBuy = (from o in orders
                             where o.OrderType == OrderTypes.BUY
                             select new 
                             {
                                 OrderType = OrderTypes.BUY.ToString(),
                                 Quantity = orders.Where(n => n.Price == o.Price
                                                       && n.OrderType == o.OrderType).Sum(s => s.Quantity),
                                 Price = o.Price
                             }).Distinct().OrderByDescending(b => b.Price);

            return summarySell.Concat(summaryBuy).Select(s => new OrderSummary { OrderType = s.OrderType, Quantity = s.Quantity, Price = s.Price });

        }

    }
}

