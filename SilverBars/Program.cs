﻿using System;
using System.Collections.Generic;
using static SilverBars.Common;

namespace SilverBars
{
    class Program
    {
        static void Main(string[] args)
        {
           
            /* 
             a) SELL: 3.5 kg for £306 [user1] 
             b) SELL: 1.2 kg for £310 [user2]  
             c) SELL: 1.5 kg for £307 [user3] 
             d) SELL: 2.0 kg for £306 [user4] 
             e) BUY : 1.0 kg for £250 [user5]
             f) BUY : 2.0 kg for £350 [user5]
             g) BUY : 3.0 kg for £350 [user6]

            */

            OrderService orderService = new OrderService();

            orderService.Register("user1", OrderTypes.SELL, 3.5, 306);
            orderService.Register("user2", OrderTypes.SELL, 1.2, 310);
            orderService.Register("user3", OrderTypes.SELL, 1.5, 307);
            orderService.Register("user4", OrderTypes.SELL, 2.0, 306);
            orderService.Register("user5", OrderTypes.BUY, 1.0, 250);
            orderService.Register("user5", OrderTypes.BUY, 2.0, 350);
            orderService.Register("user6", OrderTypes.BUY, 3.0, 350);

            var orderList = orderService.GetOrderSummary();

            foreach (var item in orderList)
            {
                Console.WriteLine(item.OrderType + " " + item.Quantity + " " + item.Price);
            }

            Console.WriteLine("------------ After Cancellation ------");

            orderService.Cancel(2);
            orderService.Cancel(5);

            var orderListAfterCancellation = orderService.GetOrderSummary();

            foreach (var item in orderListAfterCancellation)
            {
                Console.WriteLine(item.OrderType + " " + item.Quantity + " " + item.Price);
            }

            Console.ReadKey();
        }
    }
}
