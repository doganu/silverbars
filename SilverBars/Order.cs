﻿using System;
using System.Collections.Generic;
using System.Text;
using static SilverBars.Common;

namespace SilverBars
{
    class Order
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public OrderTypes OrderType { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
    }
}


// user id - order quantity (e.g.: 3.5 kg) - price per kg (e.g.: £303) - order type: BUY or SELL 