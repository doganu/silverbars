﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SilverBars
{
    class OrderSummary
    {

        public string OrderType { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
    }
}
